#!/usr/bin/env python3
import sys

from pyfastogt import build_utils, system_info

class BuildRequest(build_utils.BuildRequest):
    def __init__(self, platform, arch_name, dir_path, prefix_path):
        build_utils.BuildRequest.__init__(self, platform, arch_name, dir_path, prefix_path)
        self.arch = arch_name

    def build(self, bt):
        cmake_flags = ['-DCMAKE_TOOLCHAIN_FILE=../../../cmake/go/toolchain/{0}.cmake'.format(self.platform_name()),
                       '-DCMAKE_SYSTEM_PROCESSOR={0}'.format(self.arch),
                       '-DCPACK_SUPPORT=ON']
        self._build_via_cmake_double(cmake_flags, bt)


def print_usage():
    print("Usage:\n"
          "[required] argv[1] build type(release/debug)\n"
          "[optional] argv[2] os(linux/windows/macosx)\n"
          "[optional] argv[3] arch(x86_64/aarch64/armv7l/armv6l)\n"
          "[optional] argv[4] prefix\n")


if __name__ == "__main__":
    argc = len(sys.argv)

    if argc > 1:
        build_type = sys.argv[1]
    else:
        print_usage()
        sys.exit(1)

    host_os = system_info.get_os()
    arch_host_os = system_info.get_arch_name()

    if argc > 2:
        host_os = sys.argv[2]

    if argc > 3:
        arch_host_os = sys.argv[3]

    prefix = '/usr/local'
    if argc > 4:
        prefix = sys.argv[4]

    request = BuildRequest(host_os, arch_host_os, 'build_' + host_os, prefix)
    request.build(build_type)
