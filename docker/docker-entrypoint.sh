#!/bin/sh
set -e

if [ "${1#-}" != "$1" ]; then
	set -- gofastocloud "$@"
fi

exec "$@"