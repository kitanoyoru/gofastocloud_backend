package main

import (
	"flag"
	"fmt"
	"gofastocloud_backend/app"
	"gofastocloud_backend/app/updates"
	"gofastocloud_backend/app/utils"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

type Watcher struct {
	app.IWatcher
	restart chan bool
	stop    chan bool
}

func NewWatcher() *Watcher {
	watcher := Watcher{
		restart: make(chan bool),
		stop:    make(chan bool),
	}
	return &watcher
}

func (watcher *Watcher) OnConfigUpdated() {
	watcher.restart <- true
}
func (watcher *Watcher) StopServer() {
	watcher.stop <- true
}

func daemonize() {
	if err := os.Stdin.Close(); err != nil {
		log.Fatal(err)
	}
	if err := os.Stdout.Close(); err != nil {
		log.Fatal(err)
	}
	if err := os.Stderr.Close(); err != nil {
		log.Fatal(err)
	}
}

func pidfile() {
	err := utils.WritePID()
	if err != nil {
		log.Fatalf("pid file error: %s", err.Error())
	}
}
func removepid() {
	err := utils.RemovePID()
	if err != nil {
		log.Fatalf("pid file error: %s", err.Error())
	}
}

func main() {
	var version bool
	flag.BoolVar(&version, "version", false, "display version")

	var stopflag bool
	flag.BoolVar(&stopflag, "stop", false, "stop Server")

	var daemonflag bool
	flag.BoolVar(&daemonflag, "daemon", false, "run server us daemon")

	configPath := flag.String("config", "/etc/gofastocloud.conf", "service config")

	flag.Parse()

	// version arg
	if version {
		fmt.Printf("%s version %s\n", updates.GetProject(), updates.GetVersion())
		return
	}

	loadConfig := func() *app.Config {
		cfg, err := app.LoadConfig(*configPath)
		if err != nil {
			log.Fatal("App::Initialize load config error: ", err)
		}
		return cfg
	}

	// stop arg
	if stopflag {
		conf := loadConfig()
		resp, err := http.Get(fmt.Sprintf("%s://%s/server/stop", conf.Settings.PreferredUrlScheme, conf.Settings.Host))
		if err != nil {
			log.Fatalf("stop server error: %v", err)
		}

		if resp.StatusCode != 200 {
			log.Fatalf("stop server wrong responce code: %d", resp.StatusCode)
		}
		return
	}

	cfg := loadConfig()

	w := NewWatcher()
	a := app.NewApp(*configPath, w)

	if daemonflag {
		daemonize()
	}

	pidfile()

	stoped := false
	go func() {
		done := make(chan os.Signal, 1)
		signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

		sigs_hub := make(chan os.Signal, 1)
		signal.Notify(sigs_hub, syscall.SIGHUP)

		for {
			select {
			case <-done:
				{
					// log.Infof("Recevied signal: %s", sig.String())
					stoped = true
					a.Stop()
					break
				}
			case <-sigs_hub:
				{
					// log.Info("Try to reload config")
					cfg = loadConfig()
					a.Stop()
				}
			case <-w.restart:
				cfg = loadConfig()
				a.Stop()
			case <-w.stop:
				stoped = true
				a.Stop()
			}
		}
	}()

	for !stoped {
		a.Initialize(*cfg)
		a.Run()
		a.DeInitialize()
	}
	removepid()
}
