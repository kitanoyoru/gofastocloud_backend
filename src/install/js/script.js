function createPlayer(link) {
    const newVideo = document.createElement('video');
    newVideo.id = 'videoStream';
    newVideo.className = 'video-js vjs-default-skin';
    newVideo.style.width = window.innerWidth + 'px'
    newVideo.style.height = window.innerHeight + 'px'
    newVideo.setAttribute("controls", "controls");
    newVideo.setAttribute("autoplay", "any");
    newVideo.setAttribute("muted", "muted");
    newVideo.setAttribute('data-setup', '{ "fluid": false,  "inactivityTimeout": 0}');
    document.getElementById('video').append(newVideo);
    const newSource = document.createElement('source');
    newSource.type = "application/x-mpegURL";
    newSource.src = link;
    document.getElementById('videoStream').appendChild(newSource)
    const sp = document.createElement('script');
    sp.setAttribute('type', 'text/javascript');
    sp.src = "/install/js/player.js";
    document.getElementById('videoStream').appendChild(sp);
}
