package webrtc_in

import (
	"encoding/json"
	"strings"
	"sync"
	"time"

	"gofastocloud_backend/app/common"

	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
	"gitlab.com/fastogt/gofastocloud/media"
	"gitlab.com/fastogt/gofastocloud_base"
	"gortc.io/sdp"
)

type WebRTCSettings struct {
	Stun string
	Turn string
}

type IWebRTCIn interface {
	StartStreamWithCallback(request *media.StartStreamRequest, callback gofastocloud_base.RequestCallbackT) (*gofastocloud_base.RPCId, error)
	StopStream(request *media.StopStreamRequest) (*gofastocloud_base.RPCId, error)

	WebRTCInDeinitStream(web *media.WebRTCInDeInitRequest) (*gofastocloud_base.RPCId, error)
	WebRTCInInitStream(web *media.WebRTCInInitRequest) (*gofastocloud_base.RPCId, error)
	WebRTCInSDPStream(web *media.WebRTCInSdpRequest) (*gofastocloud_base.RPCId, error)
	WebRTCInICEStream(web *media.WebRTCInIceRequest) (*gofastocloud_base.RPCId, error)
}

type WebRTCManagerWs struct {
	IWsWebRTCClientConnectionClient

	mutex             sync.Mutex
	webRTCConnections WSConnections
	webRTCSessions    WSSessions

	client  IWebRTCIn
	watcher common.IStreamWatcher

	webrtc *WebRTCSettings
}

func NewWebRTCManagerWs(client IWebRTCIn, watcher common.IStreamWatcher, webrtc *WebRTCSettings) *WebRTCManagerWs {
	manager := WebRTCManagerWs{webRTCConnections: WSConnections{}, webRTCSessions: WSSessions{}, client: client, watcher: watcher, mutex: sync.Mutex{}, webrtc: webrtc}
	return &manager
}

func (app *WebRTCManagerWs) CreateWsWebRTCClientConnection(conn *websocket.Conn) *WsWebRTCClientAuthConnection {
	return NewWsWebRTCClientAuthConnection(conn, app)
}

func (app *WebRTCManagerWs) GetWsPeerByID(sid media.StreamId, pid PeerId) *WsWebRTCClientAuthConnection {
	app.mutex.Lock()
	defer app.mutex.Unlock()
	peers, ok := app.webRTCConnections[sid]
	if ok {
		for item := range peers {
			if item.FindPeer(pid) {
				return item
			}
		}
	}
	return nil
}

func (app *WebRTCManagerWs) GetSrtreamPeersByID(sid media.StreamId) WsListConnections {
	app.mutex.Lock()
	defer app.mutex.Unlock()
	peers, ok := app.webRTCConnections[sid]
	if ok {
		return peers
	}
	return WsListConnections{}
}

func (app *WebRTCManagerWs) sessionInfoForWsWebRTCClient(session string, ws *WsWebRTCClientAuthConnection) {
	if ws == nil {
		return
	}

	streams := []media.StreamId{}
	for stream := range app.webRTCSessions[session] {
		streams = append(streams, stream.peer.Sid)
	}

	ws.SendSessionInfo(session, streams)
}

func (app *WebRTCManagerWs) addWsWebRTCClientToSession(session string, ws *WsWebRTCClientAuthConnection) {
	if ws == nil {
		return
	}

	if _, ok := app.webRTCSessions[session]; !ok {
		app.webRTCSessions[session] = WsListConnections{}
	}
	app.webRTCSessions[session][ws] = true

	sid := ws.peer.Sid
	for k := range app.webRTCSessions[session] {
		if k != ws {
			k.SendSessionEnter(session, sid)
		}
	}
}

func (app *WebRTCManagerWs) removeWsWebRTCClientToSession(session string, ws *WsWebRTCClientAuthConnection) {
	if ws == nil || ws.peer == nil {
		return
	}

	delete(app.webRTCSessions[session], ws)

	sid := ws.peer.Sid
	for k := range app.webRTCSessions[session] {
		k.SendSessionLeave(session, sid)
	}

	if len(app.webRTCSessions[session]) == 0 {
		delete(app.webRTCSessions, session)
	}
}

func (app *WebRTCManagerWs) RegisterWsWebRTCClientAuthConnection(ws *WsWebRTCClientAuthConnection, peer *PeerInfo) {
	if ws == nil || peer == nil {
		return
	}

	app.mutex.Lock()
	ws.peer = peer
	if _, ok := app.webRTCConnections[ws.peer.Sid]; !ok {
		app.webRTCConnections[ws.peer.Sid] = WsListConnections{}
	}
	app.webRTCConnections[ws.peer.Sid][ws] = true
	app.mutex.Unlock()

	//sid := ws.peer.SID
	//pid := ws.peer.ID
	//spid := media.WebRTCSubInitRequest{ConnectionId: string(pid)}
	//app.client.WebRTCInInitStream(media.NewWebRTCInInitRequest(string(sid), spid))
}

func (app *WebRTCManagerWs) UnRegisterWsWebRTCClientAuthConnection(ws *WsWebRTCClientAuthConnection) {
	if ws == nil || ws.peer == nil {
		return
	}

	for session, v := range app.webRTCSessions {
		for con := range v {
			if con == ws {
				app.removeWsWebRTCClientToSession(session, ws)
				break
			}
		}
	}

	sid := ws.peer.Sid
	pid := ws.peer.Id
	spid := media.WebRTCSubDeInitRequest{ConnectionId: string(pid)}
	msg := media.NewWebRTCInDeInitRequest(sid, spid)
	app.client.WebRTCInDeinitStream(msg)
	req := media.NewStopStreamRequest(sid, false)
	app.client.StopStream(req)
	app.mutex.Lock()
	delete(app.webRTCConnections[sid], ws)
	ws.peer = nil
	app.mutex.Unlock()
}

func (app *WebRTCManagerWs) OnWsWebRTCClientConnectionClose(ws *WsWebRTCClientAuthConnection) {
	app.UnRegisterWsWebRTCClientAuthConnection(ws)
}

func (app *WebRTCManagerWs) MessageWebRTCProcess(rawMessage []byte, w *WsWebRTCClientAuthConnection) error {
	log.Debugf("MessageWebRTCProcess in: %s", rawMessage)

	var msg WSMessage
	err := json.Unmarshal(rawMessage, &msg)
	if err != nil {
		return err
	}

	if msg.Type == kNewCommand {
		var newMsg WSNewMessageData
		err = json.Unmarshal(msg.Data, &newMsg)
		if err != nil {
			return err
		}

		pid := newMsg.Id
		sid := newMsg.StreamID
		peer := NewPeerInfo(pid, newMsg.UserAgent, sid)
		app.RegisterWsWebRTCClientAuthConnection(w, peer)
	} else if msg.Type == kOfferCommand {
		var offerMsg WSOfferMessageData
		err = json.Unmarshal(msg.Data, &offerMsg)
		if err != nil {
			return err
		}

		var s sdp.Session
		if s, err = sdp.DecodeSession([]byte(offerMsg.Description), s); err != nil {
			return err
		}

		d := sdp.NewDecoder(s)
		m := new(sdp.Message)
		if err = d.Decode(m); err != nil {
			return err
		}

		pid := offerMsg.Id
		sid := offerMsg.StreamID
		peer := app.GetWsPeerByID(sid, pid)
		if peer != nil {
			// streamConfig := makeEncode(sid, m)
			streamConfig := makeRestream(sid, m)
			data, err := json.Marshal(streamConfig)
			if err != nil {
				return err
			}

			request := media.NewStartRestreamStreamRequest(data)
			respWait := make(chan gofastocloud_base.Response)
			_, err = app.client.StartStreamWithCallback(request, func(req *gofastocloud_base.Request, resp *gofastocloud_base.Response) {
				respWait <- *resp
			})

			if err != nil {
				log.Errorf("StartStreamWithCallback: %s", err.Error())
				// notify that start have error
			} else {
				resp := <-respWait
				if resp.IsMessage() {
					if app.watcher != nil {
						app.watcher.OnStreamStarted(common.NewStreamConfig(data, streamConfig.BaseConfig))
					}
					time.Sleep(1 * time.Second)

					string_pid := string(pid)
					var webrtc *media.WebRTCProp
					if app.webrtc != nil {
						webrtc = &media.WebRTCProp{Stun: app.webrtc.Stun, Turn: app.webrtc.Turn}
					}
					spid := media.WebRTCSubInitRequest{ConnectionId: string_pid, WebRTC: webrtc}
					msg := media.NewWebRTCInInitRequest(sid, spid)
					app.client.WebRTCInInitStream(msg)

					sdp := media.SessionDescription{ConnectionId: string_pid, SdpDescription: offerMsg.Description, SdpType: offerMsg.Type}
					msgs := media.NewWebRTCInSdpRequest(sid, sdp)
					app.client.WebRTCInSDPStream(msgs)
				} else {
					// notify that start have error
				}
			}
		}
	} else if msg.Type == kCandidateCommand {
		var candidateMsg WSCandidateMessageData
		err = json.Unmarshal(msg.Data, &candidateMsg)
		if err != nil {
			return err
		}

		pid := candidateMsg.Id
		sid := candidateMsg.StreamID
		peer := app.GetWsPeerByID(sid, pid)
		if peer != nil {
			string_pid := string(pid)
			ice := media.IceCandidate{ConnectionId: string_pid, Candidate: candidateMsg.Candidate, Mlindex: candidateMsg.SdpMLineIndex, Mid: candidateMsg.SdpMid}
			msg := media.NewWebRTCInIceRequest(sid, ice)
			app.client.WebRTCInICEStream(msg)
		}
	} else if msg.Type == kSessionInfoCommand {
		var infoMsg WSSessionInfoMessageData
		err = json.Unmarshal(msg.Data, &infoMsg)
		if err != nil {
			return err
		}

		session := infoMsg.SessionID
		pid := infoMsg.Id
		sid := infoMsg.StreamID
		peer := app.GetWsPeerByID(sid, pid)
		if peer != nil {
			app.sessionInfoForWsWebRTCClient(session, peer)
		}
	} else if msg.Type == kEnterCommand {
		var enterMsg WSEnterMessageData
		err = json.Unmarshal(msg.Data, &enterMsg)
		if err != nil {
			return err
		}

		session := enterMsg.SessionID
		pid := enterMsg.Id
		sid := enterMsg.StreamID
		peer := app.GetWsPeerByID(sid, pid)
		if peer != nil {
			app.addWsWebRTCClientToSession(session, peer)
		}
	} else if msg.Type == kLeaveCommand {
		var leaveMsg WSLeaveMessageData
		err = json.Unmarshal(msg.Data, &leaveMsg)
		if err != nil {
			return err
		}

		session := leaveMsg.SessionID
		pid := leaveMsg.Id
		sid := leaveMsg.StreamID
		peer := app.GetWsPeerByID(sid, pid)
		if peer != nil {
			app.removeWsWebRTCClientToSession(session, peer)
		}
	} else if msg.Type == kByeCommand {
		var byeMsg WSByeMessageData
		err = json.Unmarshal(msg.Data, &byeMsg)
		if err != nil {
			return err
		}

		pid := byeMsg.Id
		sid := byeMsg.StreamID
		peer := app.GetWsPeerByID(sid, pid)
		if peer != nil {
			app.UnRegisterWsWebRTCClientAuthConnection(peer)
		}
	} else {
		log.Warnf("Not handled command type: %s", msg.Type)
	}

	return nil
}

func makeRestream(sid media.StreamId, m *sdp.Message) media.RelayConfig {
	var parserVideo media.VideoParser = media.H264_PARSE
	var parserAudio media.AudioParser = media.OPUS_PARSE
	haveAudio := false
	haveVideo := false
	for _, med := range m.Medias {
		rtpmap := med.Attribute("rtpmap")
		log.Debugf("SDP media: %s, rtpmap: %s", med.Description.Type, rtpmap)
		if med.Description.Type == "video" {
			haveVideo = true
			if strings.Contains(rtpmap, "VP8") {
				parserVideo = media.VP8_PARSE
			} else if strings.Contains(rtpmap, "VP9") {
				parserVideo = media.VP9_PARSE
			}
		} else if med.Description.Type == "audio" {
			haveAudio = true
		}
	}

	out := []media.OutputUri{*media.NewWebRTCOutputUri(0, nil)}
	input := []media.InputUri{*media.NewWebRTCInputUri(0, nil)}

	feedback := media.MakeFeedbackDir(sid)
	dataDir := media.MakeDataDir(sid)
	relay := media.MakeRelayConfig(sid, out, &parserVideo, &parserAudio, feedback, dataDir, media.LOG_LEVEL_INFO, false, haveVideo, haveAudio, 10, nil, input, nil, 1)
	return relay
}

func makeEncode(sid media.StreamId, m *sdp.Message) media.EncodeConfig {
	var codecVideo media.VideoCodec = media.X264_ENC
	var codecAudio media.AudioCodec = media.OPUS_ENC
	haveAudio := false
	haveVideo := false
	for _, med := range m.Medias {
		rtpmap := med.Attribute("rtpmap")
		log.Debugf("SDP media: %s, rtpmap: %s", med.Description.Type, rtpmap)
		if med.Description.Type == "video" {
			haveVideo = true
			if strings.Contains(rtpmap, "VP8") {
				codecVideo = media.VP8_ENC
			} else if strings.Contains(rtpmap, "VP9") {
				codecVideo = media.VP9_ENC
			}
		} else if med.Description.Type == "audio" {
			haveAudio = true
		}
	}

	// codecVideo = media.NV_H264_ENC
	// codecVideo = media.OPEN_H264_ENC
	// codecVideo = media.X264_ENC

	out := []media.OutputUri{*media.NewWebRTCOutputUri(0, nil)}
	input := []media.InputUri{*media.NewWebRTCInputUri(0, nil)}

	color := 32512
	back := media.BackgroundEffect{Type: media.COLOR, Color: &color}
	feedback := media.MakeFeedbackDir(sid)
	dataDir := media.MakeDataDir(sid)
	encode := media.MakeEncodeConfig(sid, out, feedback, dataDir, media.LOG_LEVEL_INFO, false, haveVideo, haveAudio, 10, nil, input, nil, 1, false, false, nil, nil, nil, codecVideo, codecAudio, nil, nil, nil, nil, nil, nil, nil, nil, nil, &back, nil, nil)
	return encode
}
