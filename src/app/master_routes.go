package app

import (
	"encoding/json"
	"gofastocloud_backend/app/errorgt"
	"net/http"

	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models"
	"gitlab.com/fastogt/gofastogt"
)

func (app *App) GetConfig(w http.ResponseWriter, r *http.Request) {
	_, err := app.masterAuth.AuthMasterHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(app.config.Settings))
}

func (app *App) PostConfig(w http.ResponseWriter, r *http.Request) {
	masterConf, err := app.masterAuth.AuthMasterHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	var newConf gofastocloud_models.GoBackendSettings
	if err := json.NewDecoder(r.Body).Decode(&newConf); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	newConf.Master = *masterConf
	config := Config{Settings: newConf}
	err = SaveConfig(&config, app.configPath)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	if app.watcher != nil {
		app.watcher.OnConfigUpdated()
	}
	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(newConf))
}

func (app *App) ServerStop(w http.ResponseWriter, r *http.Request) {
	host, _, err := gofastogt.GetIPFromRequest(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if gofastogt.IsLocalHost(*host) {
		w.WriteHeader(http.StatusOK)
		app.watcher.StopServer()
		return
	}

	w.WriteHeader(http.StatusForbidden)
}
