package app

import (
	"gofastocloud_backend/app/updates"
	"path/filepath"
)

const TEMPLATE_FOLDER string = "install/template/"

func GetTemplatePath(path string) string {
	return filepath.Join(updates.ShareAbsFolder, TEMPLATE_FOLDER, path)
}
