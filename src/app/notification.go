package app

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/fastogt/gofastocloud/media"
)

func (app *App) SendStopStreamNotification(id, name, host string, status media.ExitStatus, emails []string) {
	alarm := makeAlarmNotificationByType(app.config.Settings.AlarmNotification)
	if alarm == nil {
		return
	}
	templatePath := GetTemplatePath("stream_alarm.html")
	msg, err := alarm.BuildStopStreamMessage(id, name, host, templatePath, status)
	if err != nil {
		log.Errorf("error build alarm message. Error: %v", err.Error())
		return
	}

	err = alarm.SendMessage(msg, emails)
	if err != nil {
		log.Errorf("error send alarm message. Error: %v", err.Error())
		return
	}
}
