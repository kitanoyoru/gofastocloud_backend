package alarm_notification

import (
	"bytes"
	"errors"
	"fmt"
	"html/template"
	"net/smtp"
	"strconv"

	"gitlab.com/fastogt/gofastocloud/media"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models"
)

type EmailAlarm struct {
	IAlarm

	EmailServerSettings gofastocloud_models.EmailServerSettings
}

type EmailServerSettings struct {
	MailServer   string `yaml:"mail_server"`
	MailUser     string `yaml:"user"`
	MailPassword string `yaml:"mail_password"`
	MailPort     string `yaml:"mail_port"`
}

type EmailParam struct {
	StreamID     string
	StreamName   string
	StopedStatus string
	NodeHost     string
}

func makeStreamExitStatusForEmail(status media.ExitStatus) string {
	if status == media.EXIT_SUCCESS {
		return "Success stop"
	} else if status == media.EXIT_FAILURE {
		return "Failure stop"
	}
	return "Unknown"
}

func (e EmailAlarm) BuildStopStreamMessage(id string, name string, host string, templatePath string, status media.ExitStatus) ([]byte, error) {
	var email EmailParam
	email.StreamID = id
	email.StreamName = name
	email.NodeHost = host
	email.StopedStatus = makeStreamExitStatusForEmail(status)

	tmpl, err := template.ParseFiles(templatePath)
	if err != nil {
		return nil, err
	}

	var body bytes.Buffer
	err = tmpl.Execute(&body, email)
	if err != nil {
		return nil, err
	}

	title := fmt.Sprint("Subject: Stream status changed!\r\n" +
		"MIME: MIME-version: 1.0\r\n" +
		"Content-Type: text/html; charset=\"UTF-8\";\r\n" +
		"\r\n")

	msg := []byte(title + body.String())

	return msg, nil
}

type loginAuth struct {
	username, password string
}

// LoginAuth is used for smtp login auth
func LoginAuth(username, password string) smtp.Auth {
	return &loginAuth{username, password}
}

func (a *loginAuth) Start(server *smtp.ServerInfo) (string, []byte, error) {
	return "LOGIN", []byte(a.username), nil
}

func (a *loginAuth) Next(fromServer []byte, more bool) ([]byte, error) {
	if more {
		switch string(fromServer) {
		case "Username:":
			return []byte(a.username), nil
		case "Password:":
			return []byte(a.password), nil
		default:
			return nil, errors.New("unknown from server")
		}
	}
	return nil, nil
}

func (e EmailAlarm) SendMessage(msg []byte, adresses []string) error {
	auth := LoginAuth(e.EmailServerSettings.MailUser, e.EmailServerSettings.MailPassword)
	return smtp.SendMail(e.EmailServerSettings.MailServer+":"+strconv.Itoa(e.EmailServerSettings.MailPort),
		auth, e.EmailServerSettings.MailUser, adresses, msg)
}
