package app

import (
	"net/http"

	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
)

func (app *App) ServeWebRTCWebRTCOut(w http.ResponseWriter, r *http.Request) {
	u := websocket.Upgrader{CheckOrigin: func(r *http.Request) bool { return true }}
	c, err := u.Upgrade(w, r, nil)
	if err != nil {
		log.Errorf("ws webrtc out error: %s", err.Error())
		return
	}

	ws := app.wsOutWebRTCManager.CreateWsWebRTCClientConnection(c)

	log.Debugf("New client ws out")
	ws.WriteMessage("You are welcome to FastoCloud WebRTC out websocket!")
	defer ws.Close()

	for {
		_, message, err := c.ReadMessage()

		if err != nil {
			log.Errorf("ws webrtc out read error: %s", err.Error())
			break
		}

		err = app.wsOutWebRTCManager.MessageWebRTCProcess(message, ws)
		if err != nil {
			log.Errorf("ws webrtc out process error: %s", err.Error())
			break
		}
	}
}
