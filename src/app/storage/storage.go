package storage

import (
	"encoding/json"
)

type IStorage interface {
	GetStreams() ([]json.RawMessage, error)
	GetStream(key string) (json.RawMessage, error)
	SetStream(key string, value json.RawMessage) error
	DeleteStream(key string) error

	GetSeasons() ([]json.RawMessage, error)
	GetSeason(key string) (json.RawMessage, error)
	SetSeason(key string, value json.RawMessage) error
	DeleteSeason(key string) error

	GetSerials() ([]json.RawMessage, error)
	GetSerial(key string) (json.RawMessage, error)
	SetSerial(key string, value json.RawMessage) error
	DeleteSerial(key string) error

	Close() error
}
