package storage

import (
	"encoding/json"

	"github.com/fastogt/pudge"
)

type Pudge struct {
	IStorage

	handle        *pudge.Db
	handleSeasons *pudge.Db
	handleSerials *pudge.Db
}

func NewPudge(path, seriesPath, serialsPath string) (*Pudge, error) {
	cfg := &pudge.Config{SyncInterval: 0} // disable every second fsync
	db, err := pudge.Open(path, cfg)
	if err != nil {
		return nil, err
	}

	dbSeasons, err := pudge.Open(seriesPath, cfg)
	if err != nil {
		return nil, err
	}

	dbSerials, err := pudge.Open(serialsPath, cfg)
	if err != nil {
		return nil, err
	}

	pudge := Pudge{handle: db, handleSeasons: dbSeasons, handleSerials: dbSerials}
	return &pudge, nil
}

//streams
func (storage *Pudge) SetStream(key string, value json.RawMessage) error {
	return storage.handle.Set(key, value)
}

func (storage *Pudge) GetStream(key string) (json.RawMessage, error) {
	var stream json.RawMessage
	err := storage.handle.Get(key, &stream)
	if err != nil {
		return nil, err
	}

	return stream, nil
}

func (storage *Pudge) DeleteStream(key string) error {
	return storage.handle.Delete(key)
}

func (storage *Pudge) GetStreams() ([]json.RawMessage, error) {
	k, err := storage.handle.Keys(nil, 0, 0, false)
	if err != nil {
		return nil, err
	}

	var streams []json.RawMessage
	for _, key := range k {
		var stream json.RawMessage
		err := storage.handle.Get(key, &stream)
		if err == nil {
			streams = append(streams, stream)
		}
	}

	for i, j := 0, len(streams)-1; i < j; i, j = i+1, j-1 {
		streams[i], streams[j] = streams[j], streams[i]
	}
	return streams, nil
}

// seasons
func (storage *Pudge) SetSeason(key string, value json.RawMessage) error {
	return storage.handleSeasons.Set(key, value)
}

func (storage *Pudge) GetSeason(key string) (json.RawMessage, error) {
	var season json.RawMessage
	err := storage.handleSeasons.Get(key, &season)
	if err != nil {
		return nil, err
	}

	return season, nil
}

func (storage *Pudge) DeleteSeason(key string) error {
	return storage.handleSeasons.Delete(key)
}

func (storage *Pudge) GetSeasons() ([]json.RawMessage, error) {
	k, err := storage.handleSeasons.Keys(nil, 0, 0, false)
	if err != nil {
		return nil, err
	}

	var seasons []json.RawMessage
	for _, key := range k {
		var season json.RawMessage
		err := storage.handleSeasons.Get(key, &season)
		if err == nil {
			seasons = append(seasons, season)
		}
	}

	for i, j := 0, len(seasons)-1; i < j; i, j = i+1, j-1 {
		seasons[i], seasons[j] = seasons[j], seasons[i]
	}
	return seasons, nil
}

// serials
func (storage *Pudge) SetSerial(key string, value json.RawMessage) error {
	return storage.handleSerials.Set(key, value)
}

func (storage *Pudge) GetSerial(key string) (json.RawMessage, error) {
	var season json.RawMessage
	err := storage.handleSerials.Get(key, &season)
	if err != nil {
		return nil, err
	}

	return season, nil
}

func (storage *Pudge) DeleteSerial(key string) error {
	return storage.handleSerials.Delete(key)
}

func (storage *Pudge) GetSerials() ([]json.RawMessage, error) {
	k, err := storage.handleSerials.Keys(nil, 0, 0, false)
	if err != nil {
		return nil, err
	}

	var serials []json.RawMessage
	for _, key := range k {
		var serial json.RawMessage
		err := storage.handleSerials.Get(key, &serial)
		if err == nil {
			serials = append(serials, serial)
		}
	}
	for i, j := 0, len(serials)-1; i < j; i, j = i+1, j-1 {
		serials[i], serials[j] = serials[j], serials[i]
	}
	return serials, nil
}

func (storage *Pudge) Close() error {
	storage.handleSerials.Close()
	storage.handleSeasons.Close()
	storage.handle.Close()
	return nil
}
