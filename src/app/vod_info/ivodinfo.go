package vod_info

import "gitlab.com/fastogt/gofastogt"

type IVodInfo interface {
	SearchVodInfo(query, logo, background string) ([]VodInfoResults, error)
	GetVodInfo(id, logo, background string) (*VodInfo, error)
}

type VodInfoResults struct {
	ID            string                `json:"id"`
	Description   string                `json:"description"`
	Name          string                `json:"name"`
	PrimeDate     gofastogt.UtcTimeMsec `json:"prime_date"`
	TvgLogo       string                `json:"tvg_logo"`
	TvgBackground string                `json:"background_url"`
	UserScore     float32               `json:"user_score"`
}

type VodInfo struct {
	VodInfoResults
	Сountry  string                 `json:"country"`
	Duration gofastogt.DurationMsec `json:"duration"`
}
