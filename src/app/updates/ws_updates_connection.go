package updates

import (
	"encoding/json"
	"sync"

	"github.com/gorilla/websocket"
	"gitlab.com/fastogt/gofastocloud/media"
)

// streams
const kQuitStatusStreamBroadcastCommand = "quit_status_stream"
const kStatisticStreamBroadcastCommand = "statistic_stream"
const kChangedStreamBroadcastCommand = "changed_source_stream"
const kMlNotificationStreamBroadcastCommand = "ml_notification_stream"
const kResultStreamBroadcastCommand = "result_stream"

const kStreamDBAdded = "stream_added"
const kStreamDBUpdated = "stream_updated"
const kStreamDBRemoved = "stream_removed"

// season
const kSeasonDBAdded = "season_added"
const kSeasonDBUpdated = "season_updated"
const kSeasonDBRemoved = "season_removed"

// serial
const kSerialDBAdded = "serial_added"
const kSerialDBUpdated = "serial_updated"
const kSerialDBRemoved = "serial_removed"

// service
const kMediaStatisticServiceBroadcastCommand = "media_statistic_service"
const kStatisticServiceBroadcastCommand = "statistic_service"

type WSMessage struct {
	Type string          `json:"type"`
	Data json.RawMessage `json:"data"`
}

type IWsUpdatesClientConnectionClient interface {
	OnWsUpdatesClientConnectionClose(ws *WsUpdatesClientConnection)
}

type WsListConnections map[*WsUpdatesClientConnection]bool

type WsUpdatesClientConnection struct {
	conn      *websocket.Conn
	connMutex sync.Mutex

	client IWsUpdatesClientConnectionClient
}

type WSStreamStatisticMessage struct {
	Type      string                    `json:"type"`
	Statistic media.StreamStatisticInfo `json:"statistic"`
}

type WSServerStatisticMessage struct {
	Type      string                     `json:"type"`
	Statistic media.ServiceStatisticInfo `json:"statistic"`
}

func NewWsUpdatesClientConnection(connection *websocket.Conn, client IWsUpdatesClientConnectionClient) *WsUpdatesClientConnection {
	return &WsUpdatesClientConnection{conn: connection, client: client, connMutex: sync.Mutex{}}
}

func (ws *WsUpdatesClientConnection) WriteJSONThreadSave(wsm WSMessage) error {
	ws.connMutex.Lock()
	result := ws.conn.WriteJSON(wsm)
	ws.connMutex.Unlock()
	return result
}

func (ws *WsUpdatesClientConnection) SendStreamDBAdded(stream json.RawMessage) error {
	wsm := WSMessage{Type: kStreamDBAdded, Data: stream}
	return ws.WriteJSONThreadSave(wsm)
}

func (ws *WsUpdatesClientConnection) SendStreamDBUpdated(stream json.RawMessage) error {
	wsm := WSMessage{Type: kStreamDBUpdated, Data: stream}
	return ws.WriteJSONThreadSave(wsm)
}

func (ws *WsUpdatesClientConnection) SendStreamDBRemoved(stream json.RawMessage) error {
	wsm := WSMessage{Type: kStreamDBRemoved, Data: stream}
	return ws.WriteJSONThreadSave(wsm)
}

func (ws *WsUpdatesClientConnection) SendSeasonDBAdded(season json.RawMessage) error {
	wsm := WSMessage{Type: kSeasonDBAdded, Data: season}
	return ws.WriteJSONThreadSave(wsm)
}
func (ws *WsUpdatesClientConnection) SendSeasonDBUpdated(season json.RawMessage) error {
	wsm := WSMessage{Type: kSeasonDBUpdated, Data: season}
	return ws.WriteJSONThreadSave(wsm)
}

func (ws *WsUpdatesClientConnection) SendSeasonDBRemoved(season json.RawMessage) error {
	wsm := WSMessage{Type: kSeasonDBRemoved, Data: season}
	return ws.WriteJSONThreadSave(wsm)
}
func (ws *WsUpdatesClientConnection) SendSerialDBAdded(season json.RawMessage) error {
	wsm := WSMessage{Type: kSerialDBAdded, Data: season}
	return ws.WriteJSONThreadSave(wsm)
}
func (ws *WsUpdatesClientConnection) SendSerialDBUpdated(season json.RawMessage) error {
	wsm := WSMessage{Type: kSerialDBUpdated, Data: season}
	return ws.WriteJSONThreadSave(wsm)
}

func (ws *WsUpdatesClientConnection) SendSerialDBRemoved(season json.RawMessage) error {
	wsm := WSMessage{Type: kSerialDBRemoved, Data: season}
	return ws.WriteJSONThreadSave(wsm)
}

func (ws *WsUpdatesClientConnection) SendStreamStatistic(statistic media.StreamStatisticInfo) error {
	raw, err := json.Marshal(statistic)
	if err != nil {
		return err
	}

	wsm := WSMessage{Type: kStatisticStreamBroadcastCommand, Data: raw}
	return ws.WriteJSONThreadSave(wsm)
}

func (ws *WsUpdatesClientConnection) SendMlNotificationInfo(notify media.MlNotificationInfo) error {
	raw, err := json.Marshal(notify)
	if err != nil {
		return err
	}

	wsm := WSMessage{Type: kMlNotificationStreamBroadcastCommand, Data: raw}
	return ws.WriteJSONThreadSave(wsm)
}

func (ws *WsUpdatesClientConnection) SendServiceStatistic(statistic ServerStats) error {
	raw, err := json.Marshal(statistic)
	if err != nil {
		return err
	}

	wsm := WSMessage{Type: kStatisticServiceBroadcastCommand, Data: raw}
	return ws.WriteJSONThreadSave(wsm)
}

func (ws *WsUpdatesClientConnection) SendMediaServiceStatistic(statistic media.ServiceStatisticInfo) error {
	raw, err := json.Marshal(statistic)
	if err != nil {
		return err
	}

	wsm := WSMessage{Type: kMediaStatisticServiceBroadcastCommand, Data: raw}
	return ws.WriteJSONThreadSave(wsm)
}

func (ws *WsUpdatesClientConnection) SendStreamChangedSources(source media.ChangedSourcesInfo) error {
	raw, err := json.Marshal(source)
	if err != nil {
		return err
	}

	wsm := WSMessage{Type: kChangedStreamBroadcastCommand, Data: raw}
	return ws.WriteJSONThreadSave(wsm)
}

func (ws *WsUpdatesClientConnection) SendStreamResult(source media.ResultStreamInfo) error {
	raw, err := json.Marshal(source)
	if err != nil {
		return err
	}

	wsm := WSMessage{Type: kResultStreamBroadcastCommand, Data: raw}
	return ws.WriteJSONThreadSave(wsm)
}

func (ws *WsUpdatesClientConnection) SendStreamQuitStatus(status media.QuitStatusInfo) error {
	raw, err := json.Marshal(status)
	if err != nil {
		return err
	}

	wsm := WSMessage{Type: kQuitStatusStreamBroadcastCommand, Data: raw}
	return ws.WriteJSONThreadSave(wsm)
}

func (ws *WsUpdatesClientConnection) WriteMessage(message string) error {
	data := map[string]interface{}{"message": message}
	ws.connMutex.Lock()
	result := ws.conn.WriteJSON(data)
	ws.connMutex.Unlock()
	return result
}

func (ws *WsUpdatesClientConnection) Close() error {
	if ws.client != nil {
		ws.client.OnWsUpdatesClientConnectionClose(ws)
	}
	return ws.conn.Close()
}
